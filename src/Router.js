import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
} from 'react-native';

import Login from '../src/screen/Login';
import List from '../src/screen/List';
import Insert from '../src/screen/Insert';
import Update from '../src/screen/Update';
import InsertSql from '../src/screen/InsertSql';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import io from 'socket.io-client/dist/socket.io';
import PushNotification from 'react-native-push-notification';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
const {width, height} = Dimensions.get('window');

export default class Router extends Component {
  state = {
    message: null,
  };
  componentDidMount() {
    const socket = io('http://84.54.13.23:80/', {
      transports: ['websocket'],
    });

    socket.on('connect', () => {
      console.log('socket connected');
      socket.on(
        'update',
        function(response) {
          this.setState({message: response.data.message});
          PushNotification.localNotification({
            title: 'Socket Push Notification', // (optional)
            message: response.data.message, // (required)
          });
        }.bind(this),
      );
    });

    socket.on('connect_error', err => {
      console.log(err);
    });

    socket.on('disconnect', () => {
      console.log('Disconnected Socket!');
    });
  }
  render() {
    return <AppContainer />;
  }
}
const AppNavigator = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: {
        headerShown: false,
      },
    },
    List: {
      screen: List,
      navigationOptions: {
        headerLeft: null,
      },
    },
    Insert: {
      screen: Insert,
      navigationOptions: {
        headerTitle: 'Öğrenci Ekle',
      },
    },
    InsertSql: {
      screen: InsertSql,
      navigationOptions: {
        headerTitle: 'Öğrenci Ekle Sqlite',
      },
    },
    Update: {
      screen: Update,
      navigationOptions: {
        headerTitle: 'Öğrenci Güncelle',
      },
    },
  },
  {
    headerLayoutPreset: 'center',
    initialRouteName: 'Login',
  },
);

PushNotification.configure({
  // (optional) Called when Token is generated (iOS and Android)
  onRegister: function(token) {
    console.log('TOKEN:', token);
  },

  // (required) Called when a remote or local notification is opened or received
  onNotification: function(notification) {
    console.log('NOTIFICATION:', notification);

    // process the notification

    // required on iOS only (see fetchCompletionHandler docs: https://github.com/react-native-community/react-native-push-notification-ios)
    notification.finish(PushNotificationIOS.FetchResult.NoData);
  },

  // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
  senderID: 'YOUR GCM (OR FCM) SENDER ID',

  // IOS ONLY (optional): default: all - Permissions to register.
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },

  // Should the initial notification be popped automatically
  // default: true
  popInitialNotification: true,

  /**
   * (optional) default: true
   * - Specified if permissions (ios) and token (android and ios) will requested or not,
   * - if not, you must call PushNotificationsHandler.requestPermissions() later
   */
  requestPermissions: true,
});

const AppContainer = createAppContainer(AppNavigator);
